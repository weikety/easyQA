CREATE TABLE `easyqa`.`weibo_user_tb`( 
   `openid` INT UNSIGNED NOT NULL COMMENT '微博用户ID', 
   `user_id` INT UNSIGNED NULL COMMENT '用户ID', 
   `nickname` CHAR(32) NULL COMMENT '用户昵称',
   `access_token` CHAR(32) NOT NULL , 
   `signup_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间(首次使用微博登录时间)', 
   `signin_time` DATETIME NOT NULL COMMENT '最近使用微博登录时间', 
   PRIMARY KEY `openid_pk` (`openid`),
   UNIQUE INDEX `userId_uq` (`user_id`)
 ) ENGINE=MYISAM COMMENT='微博登录用户表' CHARSET=utf8 COLLATE=utf8_general_ci;