<?php require_once VIEWPATH . "$theme_id/inc/header.inc.php";?>
<div class="main layui-clear">
    <div class="wrap">
        <div class="content detail pr">
            <a href="/u/home/<?=$article['user_id']?>" title="<?=$article['nickname']?>"><img class="avatar" src="<?=create_avatar_url($article['user_id'], $article['avatar_ext'])?>"></a>
            <h1><?=xss_filter($article['article_title'])?></h1>
            <div class="fly-tip fly-detail-hint">
                <div class="mr10 dib">
                    <span class="fly-tip-attile_type_<?=$article['article_type']?>"><?=$config['enum_show']['article_type_text'][$article['article_type']]?></span>
                    <?php if ($article['is_top'] == 2): ?>
                        <span class="fly-tip-stick">置顶</span>
                    <?php endif;?>
                    <?php if ($article['is_fine'] == 2): ?>
                        <span class="fly-tip-jing">精帖</span>
                    <?php endif;?>
                    <?php if ($article['article_type'] == 1): ?>
                        <?php if ($article['article_status'] == 2): ?>
                            <span class="fly-tip-jie">已采纳</span>
                        <?php else: ?>
                            <span>未采纳</span>
                        <?php endif;?>
                    <?php endif;?>
                </div>
                <div class="votes<?=!empty($article['vote_type']) ? ' voted' : ''?>">
                    <a class="vote vote_counts" vote_counts="<?=$article['vote_counts']?>" href="javascript:;" title="综合得票<?=$article['vote_counts']?>"><i class="my_iconfont"><?=$article['vote_counts'] > 0 ? '+' : ''?><?=$article['vote_counts']?></i></a>
                    <a class="vote<?=isset($article['vote_type']) && $article['vote_type'] == 1 ? ' active' : ''?>" href="javascript:;" article_id="<?=$article['id']?>" vote_up_counts="<?=$article['vote_up_counts']?>"<?=empty($article['vote_type']) ? ' onclick="article_vote(this, 1);"' : ''?> title="<?=$article['vote_up_counts']?>人支持<?=isset($article['vote_type']) && $article['vote_type'] == 1 ? '，您已支持' : ''?>">
                        <i class="my_iconfont">&#xe618;</i>
                    </a>
                    <a class="vote<?=isset($article['vote_type']) && $article['vote_type'] == 2 ? ' active' : ''?>" href="javascript:;" article_id="<?=$article['id']?>" vote_down_counts="<?=$article['vote_down_counts']?>"<?=empty($article['vote_type']) ? ' onclick="article_vote(this, 2);"' : ''?> title="<?=$article['vote_down_counts']?>人反对<?=isset($article['vote_type']) && $article['vote_type'] == 2 ? '，您已反对' : ''?>">
                        <i class="my_iconfont">&#xeefe;</i>
                    </a>
                </div>
                <a href="/u/home/<?=$article['user_id']?>">
                    <?=$article['nickname']?>
                    <?=time_tran($article['add_time'])?>发布
                </a>
                <?php if (isset($user) && $user['id'] == $article['user_id']): ?>
                    <a class="jie-admin" href="/article/edit/<?=$article['id']?>">编辑此文</a>
                <?php endif;?>
                <div class="fly-list-hint">
                    <i class="iconfont" title="回答">&#xe60c;</i> <?=$article['comment_counts']?>
                    <i class="iconfont" title="人气">&#xe60b;</i> <?=$article['view_counts']?>
                </div>
            </div>

            <div class="detail-body" style="margin-bottom: 20px;">
                <?=html_newline(content_xss_filter($article['article_content']))?>
                <?php if (is_array($article_append_lists)): ?>
                    <div class="article_append_lists" style="margin-bottom: 20px;">
                        <?php foreach ($article_append_lists as $_article_append): ?>
                            <div class="article_append">
                                <div class="t_graw mb5">
                                    <span>追加内容</span>
                                    <span><?=time_tran($_article_append['append_time'])?></span>
                                </div>
                                <div><?=html_newline(content_xss_filter($_article_append['append_content']))?></div>
                            </div>
                        <?php endforeach;?>
                    </div>
                <?php endif;?>
            </div>

            <a name="comment"></a>
            <h2 class="page-title">热忱回答<span>（<em id="jiedaCount"><?=$comment_counts?></em>）</span></h2>

            <ul class="jieda">
                <?php if (is_array($comment_lists)): ?>
                    <?php foreach ($comment_lists as $_comment): ?>
                        <li class="jieda-daan">
                            <div class="detail-about detail-about-reply">
                                <a class="jie-user" href="/u/home/<?=$_comment['user_id']?>">
                                    <img src="<?=create_avatar_url($_comment['user_id'], $_comment['avatar_ext'])?>">
                                </a>
                            </div>
                            <div class="detail-body jieda-body">
                                <?php if ($_comment['comment_status'] == 2): ?>
                                    <i class="iconfont icon-caina" title="最佳答案"></i>
                                <?php endif;?>
                                <div class="mb5">
                                    <div class="votes<?=!empty($_comment['vote_type']) ? ' voted' : ''?>">
                                        <a class="vote vote_counts" vote_counts="<?=$_comment['vote_counts']?>" href="javascript:;" title="综合得票<?=$_comment['vote_counts']?>"><i class="my_iconfont"><?=$_comment['vote_counts'] > 0 ? '+' : ''?><?=$_comment['vote_counts']?></i></a>
                                        <a class="vote<?=isset($_comment['vote_type']) && $_comment['vote_type'] == 1 ? ' active' : ''?>" href="javascript:;" comment_id="<?=$_comment['id']?>" vote_up_counts="<?=$_comment['vote_up_counts']?>"<?=empty($_comment['vote_type']) ? ' onclick="comment_vote(this, 1);"' : ''?> title="<?=$_comment['vote_up_counts']?>人支持<?=isset($_comment['vote_type']) && $_comment['vote_type'] == 1 ? '，您已支持' : ''?>">
                                            <i class="my_iconfont">&#xe618;</i>
                                        </a>
                                        <a class="vote<?=isset($_comment['vote_type']) && $_comment['vote_type'] == 2 ? ' active' : ''?>" href="javascript:;" comment_id="<?=$_comment['id']?>" vote_down_counts="<?=$_comment['vote_down_counts']?>"<?=empty($_comment['vote_type']) ? ' onclick="comment_vote(this, 2);"' : ''?> title="<?=$_comment['vote_down_counts']?>人反对<?=isset($_comment['vote_type']) && $_comment['vote_type'] == 2 ? '，您已反对' : ''?>">
                                            <i class="my_iconfont">&#xeefe;</i>
                                        </a>
                                    </div>
                                    <a href="/u/home/<?=$_comment['user_id']?>"><?=$_comment['nickname']?></a>
                                    <?php if ($article['user_id'] == $_comment['user_id']): ?>
                                        <em>(楼主)</em>
                                    <?php endif;?>
                                </div>
                                <?=html_newline(content_xss_filter($_comment['comment_content']))?>
                            </div>
                            <div class="jieda-reply">
                                <span class="time"><?=time_tran($_comment['add_time'])?></span>
                                <?php if (!empty($_comment['dialog_id'])): ?>
                                    <a href="javascript:;" dialog_id="<?=$_comment['dialog_id']?>" onclick="dialog_show(this);"><i class="my_iconfont">&#xe792;</i>查看对话</a>
                                <?php endif;?>
                                <a href="javascript:;" comment_id="<?=$_comment['id']?>" dialog_id="<?=$_comment['dialog_id']?>" nickname="<?=$_comment['nickname']?>" onclick="comment_reply_show(this);"><i class="my_iconfont">&#xe619;</i>回复</a>
                                <?php if (isset($user) && $article['user_id'] == $user['id'] && $article['article_type'] == 1 && $article['article_status'] != 2): ?>
                                    <a class="accept_btn" href="javascript:;" onclick="comment_accept(<?=$_comment['id']?>);">采纳</a>
                                <?php endif;?>
                            </div>
                        </li>
                    <?php endforeach;?>
                <?php else: ?>
                    <li class="fly-none">没有任何回答</li>
                <?php endif;?>
            </ul>

            <?php if (isset($user)): ?>
                <div class="layui-form layui-form-pane">
                    <form method="post" comment_id="" dialog_id="" onsubmit="return comment_add(this);">
                        <div id="comment_rich_editor"></div>
                        <div class="layui-form-item">
                            <button type="submit" class="layui-btn">提交回答</button>
                        </div>
                    </form>
                </div>
            <?php else: ?>
                <div>评论请先<a class="t_orange" href="/account/signin">登录</a></div>
            <?php endif;?>
        </div>
    </div>
    <div class="edge">
        <?php require_once VIEWPATH . "$theme_id/inc/q_by_view_hot_lists.inc.php";?>
        <?php require_once VIEWPATH . "$theme_id/inc/q_by_comment_hot_lists.inc.php";?>
    </div>
</div>

<link rel="stylesheet" href="<?=$config['files']['web']['highlight.css']?>">
<script src="<?=$config['files']['web']['highlight.js']?>"></script>
<script type="text/javascript">
var article_id = <?=$article['id']?>;

//代码高度
hljs.initHighlightingOnLoad();
$(function(){
    $('pre>code').each(function(i, block) {
        hljs.highlightBlock(block);
    });
});

//相册
layer.photos({
    photos: '.photo'
    ,zIndex: 9999999999,
    shift: 5,
    shade: [0.3, '#000000']
});

//创建富文本编辑器
$(function(){
    create_rich_editor('comment_rich_editor', '', '请输入回复内容', 260, false);
});

//采纳答案
function comment_accept(comment_id){
    layer.confirm('确定采纳这条答案吗？', {icon: 3, shade:0, title:'提示'}, function(index){
        layer.load();
        $.post(
            '/api/comment/accept',
            {
                comment_id: comment_id
            },
            function(json){
                layer.closeAll('loading');
                if(json.error_code == 'ok'){
                    layer.msg('成功采纳答案');
                    setTimeout(
                        function(){
                            document.location = document.location;
                        },
                        1500
                    );
                }
                else{
                    show_error(json.error_code);
                }
            },
            'json'
        );
        layer.close(index);
    });
}
</script>
<?php require_once VIEWPATH . "$theme_id/inc/footer.inc.php";?>