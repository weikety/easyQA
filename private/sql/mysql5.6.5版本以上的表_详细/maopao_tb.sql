CREATE TABLE `easyqa`.`maopao_tb`(
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '行唯一id',
  `maopao_content` TEXT NOT NULL COMMENT '冒泡内容',
  `view_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '浏览数',
  `comment_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论数',
  `vote_counts` INT NOT NULL DEFAULT 0 COMMENT '投票数,可以为负',
  `vote_up_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '投票数,顶数',
  `vote_down_counts` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT '投票数,踩数',
  `user_id` INT UNSIGNED NOT NULL COMMENT '用户id',
  `add_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '冒泡添加时间',
  PRIMARY KEY `id_pk` (`id`),
  INDEX `viewCounts_index` (`view_counts`),
  INDEX `commentCounts_index` (`comment_counts`),
  INDEX `voteCounts_index` (`vote_counts`),
  INDEX `userId_index` (`user_id`),
  INDEX `addTime_index` (`add_time`)
) ENGINE=INNODB COMMENT='冒泡表' CHARSET=utf8 COLLATE=utf8_general_ci;