<?php require_once VIEWPATH . "$theme_id/inc/header.inc.php";?>
<div class="main layui-clear">
    <div class="wrap">
        <div class="content">
            <?php require_once VIEWPATH . "$theme_id/u/inc/nav.inc.php";?>
            <div class="user-mine">
                <div class="layui-form layui-form-pane">
                    <?php if (!empty($github_user)): ?>
                        <p>
                            <span>已绑定Github账号：<?=$github_user['nickname']?></span>
                            <a href="javascript:;" ref="github" ref_name="Github" onclick="account_unbind(this);">解除绑定</a>
                        </p>
                    <?php endif;?>

                    <?php if (!empty($weixin_user)): ?>
                        <p>
                            <span>已绑定微信账号：<?=$weixin_user['nickname']?></span>
                            <a href="javascript:;" ref="weixin" ref_name="微信" onclick="account_unbind(this);">解除绑定</a>
                        </p>
                    <?php endif;?>

                    <?php if (!empty($qc_user)): ?>
                        <p>
                            <span>已绑定QQ账号：<?=$qc_user['nickname']?></span>
                            <a href="javascript:;" ref="qc" ref_name="QQ" onclick="account_unbind(this);">解除绑定</a>
                        </p>
                    <?php endif;?>

                    <?php if (!empty($weibo_user)): ?>
                        <p>
                            <span>已绑定微博账号：<?=$weibo_user['nickname']?></span>
                            <a href="javascript:;" ref="weibo" ref_name="微博" onclick="account_unbind(this);">解除绑定</a>
                        </p>
                    <?php endif;?>

                    <p class="open_signup_btns mt30">
                        <?php if (empty($github_user)): ?>
                            <a class="github" href="/account/github" title="使用Github账号登录"><i class="my_iconfont">&#xe735;</i></a>
                        <?php endif;?>

                        <?php if (empty($weixin_user)): ?>
                            <a class="weixin" href="/account/weixin" title="使用微信账号登录"><i class="my_iconfont">&#xe636;</i></a>
                        <?php endif;?>

                        <?php if (empty($qc_user)): ?>
                            <a class="qq_connect" href="/account/qq_connect" title="使用QQ账号登录"><i class="my_iconfont">&#xe616;</i></a>
                        <?php endif;?>

                        <?php if (empty($weibo_user)): ?>
                            <a class="weibo" href="/account/weibo" title="使用微博账号登录"><i class="my_iconfont">&#xe89c;</i></a>
                        <?php endif;?>
                    </p>
                </div>
            </div>
            <div id="LAY-page"></div>
        </div>
    </div>
    <?php require_once VIEWPATH . "$theme_id/u/inc/sidebar.inc.php";?>
</div>
<script type="text/javascript">
//解除账号关联绑定
function account_unbind(This){
    var $this = $(This);
    var ref = $this.attr('ref');
    var ref_name = $this.attr('ref_name');

    var confirm_tip = '确定要解除绑定此【' + ref_name + '】账号吗？';
    layer.confirm(confirm_tip, {icon: 3, shade:0, title:'提示'}, function(index){
        $.post(
            '/api/account/unbind',
            {
                ref: ref
            },
            function(json){
                if(json.error_code == 'ok'){
                    $this.parent('p').remove();
                    layer.msg('成功解除绑定。');
                    setTimeout(function(){
                        document.location = document.location;
                    }, 1500);
                }
                else{
                    show_error(json.error_code);
                }
            },
            'json'
        );
        layer.close(index);
    });
}
</script>
<?php require_once VIEWPATH . "$theme_id/inc/footer.inc.php";?>